// Modification of our inheritance example. 
// Derived classes now inherit from two base classes

#include <iostream>
#include <string>

using namespace std;

// BASE classes:  UCLA_Person and To_Console
class UCLA_Person{
   protected:
      string name;

   public:
      UCLA_Person( string n ) : name(n) {}
};


class To_Console{
   public:
      static void print( string s ){
         cout << s << "\n";
      }
};



// DERIVED classes: Student and Professor
class UCLA_Student : public UCLA_Person, public To_Console {
   public:
      UCLA_Student( string n ) : UCLA_Person(n) {}

      string id_card() const  {
         return "Student: " + name;
      }
};

class UCLA_Professor :  public UCLA_Person, public To_Console {
   public:
      UCLA_Professor( string n ) : UCLA_Person(n) {}

      string id_card() const  {
         return "Professor: " + name;
      }
};

/**
   In main we simply
   - Create UCLA_Person objects via constructors 
   - Display their info to console via second base class
*/
int main(){
   UCLA_Student s("Elmer Homero Petatero");
   UCLA_Professor p("Aquiles Esquivel Madrazo");

   s.print( s.id_card() );
   To_Console::print( p.id_card() );

   return 0;
}

/**
    Why do we use (prefer?) the notation
       SomeClass::static_member
    on line 57?
*/
