// Example that illustrates which constructor 
// is called on derived classes

#include <iostream>

using namespace std;

// BASE class provides two constructors: default & one-parameter
class BaseClass { 
   public: 
      BaseClass(){ 
         cout << "BASE constr: no params\n"; 
      }
      BaseClass( int a ){ 
         cout << "BASE constr: int param\n"; 
      }
};

// Derived1 inherits the default constructor
// and uses it in its provided one-parameter constructor
class Derived1 : public BaseClass { 
   public: 
      Derived1( int a ){
         cout << "Derived1 constr: int param\n\n"; 
      }
};

// Derived2 DOES inherit the default constructor
// but it does not use it.
class Derived2 : public BaseClass { 
   public: 
      Derived2( int a ) : BaseClass( a ) {
         cout << "Derived2 constr: int param\n\n"; 
      }
};

int main () {
  Derived1 d1(0);
  Derived2 d2(0);
  
  return 0;
}
