#include <iostream>
#include <vector>
#include <deque>
#include <list>
#include <map>

using namespace std;

int main(){

   // Let us start with a simple array.
   const int ARRAY_SIZE = 5;
   int a[] = { 1, 4, 6, 7, 0 };


   // Next, we display the contents using array notation ...
   cout << "Contents of array using a for loop:\n";
   for (size_t i = 0; i < ARRAY_SIZE; i++ )
      cout << a[i] << " ";
   cout << "\n";

   // ...as well as using pointer arithmentic. 
   cout << "We can also display the contents using a pointer:\n";
   for (size_t i = 0; i < ARRAY_SIZE; i++){
      int *p = &a[0];
      cout << *(p + i) << " ";
   }
   cout << "\n";



   /**
      Enough with arrays, let us initialize other containers. E.g.,
      vector, deque, list and map.
   */

   vector<int> v;
   deque<int> d;
   list<int> l;
   map<size_t, int> m;

   // Populate the containers with the contents of the array.
   // Take advatage of push_back() which is available for most  
   // of these containers
   for ( size_t i = 0 ; i < ARRAY_SIZE ; i++ ){ // <-- size_ t???
      v.push_back(a[i]);
      d.push_back(a[i]);
      l.push_back(a[i]);
      m[i] = a[i];
   }
   cout << "Now we also have a vector, a deque, a list, and a map.\n\n"; 
   

   /** 
       NOTE:
       We can do better. We should let the compiler figure out 
       the types of the objects/variables we are requesting.

       Idea:   
       Use the 'auto' keyword.
   */

   cout << "\nEven cooler!!! Range-based loops + auto = :-D \n";
   cout << "Works for a vector:\n";
   for (auto val : v ) 
      cout << val << " ";
   cout << "\n";

   cout << "It also works for the killed-but-brought-back-to-life deque:\n";
   for (auto val : d) 
      cout << val << " ";
   cout << "\n";

   cout << "And the is-being-killed-right-now list:\n";
   for (auto& val : l) 
      val = 0;
   for (auto val : l) 
      cout << val << " ";
   cout << "\n";


   // 'auto' gets rid of the ugly reverse_iterator declaration
   // just be aware that in this case 'it' is an iterator
   // instead of an element of the vector. 
   cout << "\nWe can even use 'auto' for a [backwards] vector (reverse_iterator):\n";
   for (auto it = v.rbegin(); it != v.rend(); ++it)
      cout << *it << " ";
   cout << "\n";


   // So, what about those maps???
   cout << "It also works for a MAP... provided we know how a map works:\n";
   for ( auto it = m.begin(); it != m.end(); ++it ) {
      // cout << (*it).second << " "; // <-- dereference it and request field: 'second'
      cout << it->second << " ";
   }
   cout << "\n";

   // Turns out, it was us after all.
   // The 'trick' is to request the SECOND element in the pair.

   return 0;
}

